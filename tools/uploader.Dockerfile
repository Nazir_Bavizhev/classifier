FROM golang:1.10.3


WORKDIR /go/src/bitbucket.org/Axxonsoft/classifier
COPY . /go/
RUN go get -t github.com/tools/godep
RUN /go/bin/godep restore -v

# build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /res/uploader bitbucket.org/Axxonsoft/classifier/uploader/cmd/uploader-server


FROM alpine:latest

# add certificates for https connections
RUN apk --no-cache add ca-certificates

# copy
COPY --from=0 /res/uploader /bin
