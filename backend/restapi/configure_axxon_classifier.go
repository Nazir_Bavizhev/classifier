// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"github.com/go-openapi/runtime/security"
	"io"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations/media"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations/search"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations/video_source"
)

//go:generate swagger generate server --target ../backend --name  --spec ../tools/backend.swagger.yml

func configureFlags(api *operations.AxxonClassifierAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.AxxonClassifierAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.JSONConsumer = runtime.JSONConsumer()

	api.MultipartformConsumer = runtime.DiscardConsumer

	api.JSONProducer = runtime.JSONProducer()

	api.ImageJpegProducer = runtime.ProducerFunc(func(w io.Writer, data interface{}) error {
		return errors.NotImplemented("imageJpeg producer has not yet been implemented")
	})
	api.ImageJpgProducer = runtime.ProducerFunc(func(w io.Writer, data interface{}) error {
		return errors.NotImplemented("imageJpg producer has not yet been implemented")
	})
	api.ImagePngProducer = runtime.ProducerFunc(func(w io.Writer, data interface{}) error {
		return errors.NotImplemented("imagePng producer has not yet been implemented")
	})

	// Applies when the "authToken" query is set
	api.APIKeyAuth = AuthFunc
	// Applies when the "X-Auth-Token" header is set
	api.TokenHeaderAuth = AuthFunc

	// Set your custom authorizer if needed. Default one is security.Authorized()
	// Expected interface runtime.Authorizer
	//
	// Example:
	api.APIAuthorizer = security.Authorized()
	api.FileFileUploadHandler = FileFileUploadHandler
	api.MediaImageGetHandler = media.ImageGetHandlerFunc(func(params media.ImageGetParams) middleware.Responder {
		return middleware.NotImplemented("operation media.ImageGet has not yet been implemented")
	})
	api.SearchSearchAutoResultsHandler = search.SearchAutoResultsHandlerFunc(func(params search.SearchAutoResultsParams) middleware.Responder {
		return middleware.NotImplemented("operation search.SearchAutoResults has not yet been implemented")
	})
	api.SearchSearchAutoStartHandler = search.SearchAutoStartHandlerFunc(func(params search.SearchAutoStartParams) middleware.Responder {
		return middleware.NotImplemented("operation search.SearchAutoStart has not yet been implemented")
	})
	api.SearchSearchFaceResultsHandler = search.SearchFaceResultsHandlerFunc(func(params search.SearchFaceResultsParams) middleware.Responder {
		return middleware.NotImplemented("operation search.SearchFaceResults has not yet been implemented")
	})
	api.SearchSearchFaceStartHandler = search.SearchFaceStartHandlerFunc(func(params search.SearchFaceStartParams) middleware.Responder {
		return middleware.NotImplemented("operation search.SearchFaceStart has not yet been implemented")
	})
	api.SearchSearchListHandler = search.SearchListHandlerFunc(func(params search.SearchListParams) middleware.Responder {
		return middleware.NotImplemented("operation search.SearchList has not yet been implemented")
	})
	api.SearchSearchVMDAResultsHandler = search.SearchVMDAResultsHandlerFunc(func(params search.SearchVMDAResultsParams) middleware.Responder {
		return middleware.NotImplemented("operation search.SearchVMDAResults has not yet been implemented")
	})
	api.SearchSearchVMDAStartHandler = search.SearchVMDAStartHandlerFunc(func(params search.SearchVMDAStartParams) middleware.Responder {
		return middleware.NotImplemented("operation search.SearchVMDAStart has not yet been implemented")
	})
	api.UserUserCreateHandler = UserUserCreateHandler
	api.UserUserLoginHandler = UserUserLoginHandler
	api.UserUserGetHandler = UserUserGetHandler
	api.VideoSourceVideoSourceCreateHandler = VideoSourceVideoSourceCreateHandler

	api.VideoSourceVideoSourceIndexHandler = video_source.VideoSourceIndexHandlerFunc(func(params video_source.VideoSourceIndexParams) middleware.Responder {
		return middleware.NotImplemented("operation video_source.VideoSourceIndex has not yet been implemented")
	})
	api.VideoSourceVideoSourceListHandler = VideoSourceVideoSourceListHandler

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
