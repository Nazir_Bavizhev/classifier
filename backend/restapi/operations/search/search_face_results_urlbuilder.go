// Code generated by go-swagger; DO NOT EDIT.

package search

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"errors"
	"net/url"
	golangswaggerpaths "path"
	"strings"

	"github.com/go-openapi/swag"
)

// SearchFaceResultsURL generates an URL for the search face results operation
type SearchFaceResultsURL struct {
	SearchID string

	Limit  *int64
	Offset *int64

	_basePath string
	// avoid unkeyed usage
	_ struct{}
}

// WithBasePath sets the base path for this url builder, only required when it's different from the
// base path specified in the swagger spec.
// When the value of the base path is an empty string
func (o *SearchFaceResultsURL) WithBasePath(bp string) *SearchFaceResultsURL {
	o.SetBasePath(bp)
	return o
}

// SetBasePath sets the base path for this url builder, only required when it's different from the
// base path specified in the swagger spec.
// When the value of the base path is an empty string
func (o *SearchFaceResultsURL) SetBasePath(bp string) {
	o._basePath = bp
}

// Build a url path and query string
func (o *SearchFaceResultsURL) Build() (*url.URL, error) {
	var result url.URL

	var _path = "/search/face/{searchId}"

	searchID := o.SearchID
	if searchID != "" {
		_path = strings.Replace(_path, "{searchId}", searchID, -1)
	} else {
		return nil, errors.New("SearchID is required on SearchFaceResultsURL")
	}

	_basePath := o._basePath
	if _basePath == "" {
		_basePath = "/classifier/api/v1"
	}
	result.Path = golangswaggerpaths.Join(_basePath, _path)

	qs := make(url.Values)

	var limit string
	if o.Limit != nil {
		limit = swag.FormatInt64(*o.Limit)
	}
	if limit != "" {
		qs.Set("limit", limit)
	}

	var offset string
	if o.Offset != nil {
		offset = swag.FormatInt64(*o.Offset)
	}
	if offset != "" {
		qs.Set("offset", offset)
	}

	result.RawQuery = qs.Encode()

	return &result, nil
}

// Must is a helper function to panic when the url builder returns an error
func (o *SearchFaceResultsURL) Must(u *url.URL, err error) *url.URL {
	if err != nil {
		panic(err)
	}
	if u == nil {
		panic("url can't be nil")
	}
	return u
}

// String returns the string representation of the path with query string
func (o *SearchFaceResultsURL) String() string {
	return o.Must(o.Build()).String()
}

// BuildFull builds a full url with scheme, host, path and query string
func (o *SearchFaceResultsURL) BuildFull(scheme, host string) (*url.URL, error) {
	if scheme == "" {
		return nil, errors.New("scheme is required for a full url on SearchFaceResultsURL")
	}
	if host == "" {
		return nil, errors.New("host is required for a full url on SearchFaceResultsURL")
	}

	base, err := o.Build()
	if err != nil {
		return nil, err
	}

	base.Scheme = scheme
	base.Host = host
	return base, nil
}

// StringFull returns the string representation of a complete url
func (o *SearchFaceResultsURL) StringFull(scheme, host string) string {
	return o.Must(o.BuildFull(scheme, host)).String()
}
