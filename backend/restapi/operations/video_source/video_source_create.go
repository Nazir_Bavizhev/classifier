// Code generated by go-swagger; DO NOT EDIT.

package video_source

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"
)

// VideoSourceCreateHandlerFunc turns a function with the right signature into a video source create handler
type VideoSourceCreateHandlerFunc func(VideoSourceCreateParams, interface{}) middleware.Responder

// Handle executing the request and returning a response
func (fn VideoSourceCreateHandlerFunc) Handle(params VideoSourceCreateParams, principal interface{}) middleware.Responder {
	return fn(params, principal)
}

// VideoSourceCreateHandler interface for that can handle valid video source create params
type VideoSourceCreateHandler interface {
	Handle(VideoSourceCreateParams, interface{}) middleware.Responder
}

// NewVideoSourceCreate creates a new http.Handler for the video source create operation
func NewVideoSourceCreate(ctx *middleware.Context, handler VideoSourceCreateHandler) *VideoSourceCreate {
	return &VideoSourceCreate{Context: ctx, Handler: handler}
}

/*VideoSourceCreate swagger:route POST /videoSources videoSource videoSourceCreate

Creates video source for user
Returns string id of the request.

*/
type VideoSourceCreate struct {
	Context *middleware.Context
	Handler VideoSourceCreateHandler
}

func (o *VideoSourceCreate) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewVideoSourceCreateParams()

	uprinc, aCtx, err := o.Context.Authorize(r, route)
	if err != nil {
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}
	if aCtx != nil {
		r = aCtx
	}
	var principal interface{}
	if uprinc != nil {
		principal = uprinc
	}

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params, principal) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
