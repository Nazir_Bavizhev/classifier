package restapi

import (
	"bitbucket.org/Axxonsoft/classifier/backend/model"
	"bitbucket.org/Axxonsoft/classifier/backend/models"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations/file"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations/user"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations/video_source"
	file2 "bitbucket.org/Axxonsoft/classifier/uploader/client/file"
	"bitbucket.org/Axxonsoft/classifier/util"
	"github.com/go-openapi/runtime/middleware"
	"log"
	"reflect"
)

var VideoSourceService model.VideoSourceService
var UserService model.UserService
var FileService model.FileService

var errFileGetNotFound = &file2.FileGetNotFound{}

func isType(err1 error, err2 error) bool {
	return reflect.TypeOf(err1) == reflect.TypeOf(err2)
}

var VideoSourceVideoSourceCreateHandler = video_source.VideoSourceCreateHandlerFunc(func(params video_source.VideoSourceCreateParams, principal interface{}) middleware.Responder {
	created, err := VideoSourceService.CreateVideoSource(principal.(*models.User), &params)
	if isType(err, errFileGetNotFound) {
		return util.NewNotFoundError("file not found")
	}
	if err != nil {
		log.Fatal(err) //TODO
	}
	return video_source.NewVideoSourceCreateCreated().WithPayload(created)
})

var VideoSourceVideoSourceListHandler = video_source.VideoSourceListHandlerFunc(func(params video_source.VideoSourceListParams, principal interface{}) middleware.Responder {
	list, err := VideoSourceService.ListVideoSources(principal.(*models.User), &params)
	if err != nil {
		log.Fatal(err) //TODO
	}
	return video_source.NewVideoSourceListOK().WithPayload(list)
})

var FileFileUploadHandler = file.FileUploadHandlerFunc(func(params file.FileUploadParams, principal interface{}) middleware.Responder {
	uploaded, err := FileService.UploadFile(params.Name, params.File)
	if err != nil {
		log.Fatal(err) //TODO
	}
	return file.NewFileUploadCreated().WithPayload(uploaded)
})

var UserUserCreateHandler = user.UserCreateHandlerFunc(func(params user.UserCreateParams) middleware.Responder {
	u, err := UserService.CreateUser(&params)
	if err != nil {
		log.Fatal(err)
	}
	return user.NewUserCreateCreated().WithPayload(u)
})

var UserUserLoginHandler = user.UserLoginHandlerFunc(func(params user.UserLoginParams) middleware.Responder {
	auth, err := UserService.LoginUser(&params)
	if err != nil {
		log.Fatal(err)
	}
	return user.NewUserLoginOK().WithPayload(auth)
})

var UserUserGetHandler = user.UserGetHandlerFunc(func(params user.UserGetParams, principal interface{}) middleware.Responder {
	u, err := UserService.GetUser(&params)
	if err != nil {
		log.Fatal(err)
	}
	if u == nil {
		return util.NewUnauthorized("user not found")
	}
	authUser := principal.(*models.User)
	if u.ID != authUser.ID {
		return util.NewUnauthorized("user not found")
	}
	return user.NewUserGetOK().WithPayload(u)
})

var AuthFunc = func(token string) (interface{}, error) {
	user, err := UserService.GetByTokenEnsure(&models.AuthToken{AuthToken: token})
	if err != nil {
		return nil, util.NewUnauthorized(err.Error())
	}
	return user, err
}
