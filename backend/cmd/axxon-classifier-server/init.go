package main

import (
	"bitbucket.org/Axxonsoft/classifier/backend/file"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi"
	"bitbucket.org/Axxonsoft/classifier/backend/store"
	"bitbucket.org/Axxonsoft/classifier/backend/user"
	"bitbucket.org/Axxonsoft/classifier/backend/videosource"
	"bitbucket.org/Axxonsoft/classifier/uploader/client"
	"bitbucket.org/Axxonsoft/classifier/util"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	UploaderAddr string `envconfig:"uploader_addr" default:"127.0.0.1:9091"`
}

func init() {
	var config Config
	err := envconfig.Process("classifier", &config)
	util.CheckErr(err, "envconfig")

	uploader := client.NewHTTPClientWithConfig(nil, &client.TransportConfig{
		Host:     config.UploaderAddr,
		BasePath: "/uploader/api/v1",
		Schemes:  []string{"http"},
	})

	stor := store.NewStore()
	restapi.VideoSourceService = videosource.NewVideoSourceService(stor, uploader)
	restapi.UserService = user.NewService(stor)
	restapi.FileService = file.NewService(stor, uploader)
}
