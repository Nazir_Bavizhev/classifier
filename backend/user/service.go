package user

import (
	"bitbucket.org/Axxonsoft/classifier/backend/model"
	"bitbucket.org/Axxonsoft/classifier/backend/models"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations/user"
	"bitbucket.org/Axxonsoft/classifier/backend/store"
	"bitbucket.org/Axxonsoft/classifier/util"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"time"
)

var errNotAuthenticated = errors.New("not authenticated")

const secret = "some secret"

func NewService(store store.Store) model.UserService {
	return &service{
		store: store,
	}
}

type service struct {
	store store.Store
}

func (s *service) GetByTokenEnsure(authToken *models.AuthToken) (*models.User, error) {
	claims, err := parseJWT(authToken.AuthToken)
	if err != nil {
		return nil, err
	}
	u, err := s.store.GetUserByID(claims.UserID)
	if err != nil {
		return nil, err
	}
	if u == nil {
		return nil, util.NewUnauthorized("user not found by id")
	}
	return &models.User{
		ID:    u.ID,
		Email: u.Email,
	}, nil
}

func (s *service) GetToken(user store.User) *models.AuthToken {
	return authTokenForUserID(user.ID)
}

func (s *service) LoginUser(params *user.UserLoginParams) (*models.AuthToken, error) {
	u, err := s.store.GetUserByEmail(params.Body.Email)
	if err != nil {
		return nil, err
	}
	if u == nil {
		return nil, errNotAuthenticated
	}
	if u.Password != params.Body.Password {
		return nil, errNotAuthenticated
	}

	return authTokenForUserID(u.ID), nil
}

func authTokenForUserID(userId string) *models.AuthToken {
	claims := &ClassifierClaims{
		UserID: userId,
	}

	token := signAndStringifyToken(newJWTToken(claims, time.Hour*100), secret)
	return &models.AuthToken{AuthToken: token}
}

func (s *service) GetUser(params *user.UserGetParams) (*models.User, error) {
	u, err := s.store.GetUserByID(params.UserID)
	if err != nil {
		return nil, err
	}
	if u == nil {
		return nil, nil
	}
	return &models.User{
		ID:    u.ID,
		Email: u.Email,
	}, nil
}

func (s *service) CreateUser(params *user.UserCreateParams) (user *models.User, err error) {
	u := &store.User{
		ID:       util.GetRandomID(),
		Email:    params.Body.Email,
		Password: params.Body.Password,
	}
	u, err = s.store.SaveUser(u)
	if err != nil {
		return nil, err
	}

	return &models.User{
		ID:    u.ID,
		Email: u.Email,
	}, nil
}

// ClassifierClaims claims in terminology of jwt just a data that serialized in jwt token
type ClassifierClaims struct {
	UserID string `json:"userId"`
	jwt.StandardClaims
}

func newJWTToken(claims *ClassifierClaims, ttl time.Duration) *jwt.Token {
	currentTime := time.Now()
	claims.StandardClaims = jwt.StandardClaims{
		ExpiresAt: currentTime.Add(ttl).Unix(),
		IssuedAt:  currentTime.Unix(),
		Issuer:    "Classifier",
	}

	return jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
}

func signAndStringifyToken(token *jwt.Token, secret string) string {
	tokenString, err := token.SignedString([]byte(secret))
	util.CheckErr(err, "token.SignedString")

	return tokenString
}

func parseJWT(tokenStr string) (*ClassifierClaims, error) {
	claims := &ClassifierClaims{}
	_, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (interface{}, error) {
		b := []byte(secret)
		return b, nil
	})
	return claims, err
}
