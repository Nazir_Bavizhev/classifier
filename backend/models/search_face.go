// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// SearchFace SearchFace face search
// swagger:model SearchFace
type SearchFace struct {

	// Accuracy of search
	Accuracy float64 `json:"accuracy,omitempty"`

	// begin time
	// Format: date-time
	BeginTime strfmt.DateTime `json:"beginTime,omitempty"`

	// created time
	// Format: date-time
	CreatedTime strfmt.DateTime `json:"createdTime,omitempty"`

	// deleted time
	// Format: date-time
	DeletedTime strfmt.DateTime `json:"deletedTime,omitempty"`

	// end time
	// Format: date-time
	EndTime strfmt.DateTime `json:"endTime,omitempty"`

	// file Id
	// Required: true
	FileID *string `json:"fileId"`

	// finished time
	// Format: date-time
	FinishedTime strfmt.DateTime `json:"finishedTime,omitempty"`

	// ID of current search
	ID string `json:"id,omitempty"`

	// VideoSourceID id of video source which search created for
	// Required: true
	IndexID *string `json:"indexId"`

	// Progress of search in 0..100
	// Required: true
	Progress *int64 `json:"progress"`

	// Status of search: in_progress | done
	// Required: true
	Status *string `json:"status"`

	// Type of current search auto, face, vmda
	Type string `json:"type,omitempty"`
}

// Validate validates this search face
func (m *SearchFace) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateBeginTime(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateCreatedTime(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateDeletedTime(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateEndTime(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateFileID(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateFinishedTime(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateIndexID(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateProgress(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateStatus(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *SearchFace) validateBeginTime(formats strfmt.Registry) error {

	if swag.IsZero(m.BeginTime) { // not required
		return nil
	}

	if err := validate.FormatOf("beginTime", "body", "date-time", m.BeginTime.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *SearchFace) validateCreatedTime(formats strfmt.Registry) error {

	if swag.IsZero(m.CreatedTime) { // not required
		return nil
	}

	if err := validate.FormatOf("createdTime", "body", "date-time", m.CreatedTime.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *SearchFace) validateDeletedTime(formats strfmt.Registry) error {

	if swag.IsZero(m.DeletedTime) { // not required
		return nil
	}

	if err := validate.FormatOf("deletedTime", "body", "date-time", m.DeletedTime.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *SearchFace) validateEndTime(formats strfmt.Registry) error {

	if swag.IsZero(m.EndTime) { // not required
		return nil
	}

	if err := validate.FormatOf("endTime", "body", "date-time", m.EndTime.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *SearchFace) validateFileID(formats strfmt.Registry) error {

	if err := validate.Required("fileId", "body", m.FileID); err != nil {
		return err
	}

	return nil
}

func (m *SearchFace) validateFinishedTime(formats strfmt.Registry) error {

	if swag.IsZero(m.FinishedTime) { // not required
		return nil
	}

	if err := validate.FormatOf("finishedTime", "body", "date-time", m.FinishedTime.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *SearchFace) validateIndexID(formats strfmt.Registry) error {

	if err := validate.Required("indexId", "body", m.IndexID); err != nil {
		return err
	}

	return nil
}

func (m *SearchFace) validateProgress(formats strfmt.Registry) error {

	if err := validate.Required("progress", "body", m.Progress); err != nil {
		return err
	}

	return nil
}

func (m *SearchFace) validateStatus(formats strfmt.Registry) error {

	if err := validate.Required("status", "body", m.Status); err != nil {
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (m *SearchFace) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *SearchFace) UnmarshalBinary(b []byte) error {
	var res SearchFace
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
