// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// Rectangle Rectangle in relative to image size values
// swagger:model Rectangle
type Rectangle struct {

	// bottom
	Bottom float64 `json:"bottom,omitempty"`

	// left
	Left float64 `json:"left,omitempty"`

	// right
	Right float64 `json:"right,omitempty"`

	// top
	Top float64 `json:"top,omitempty"`
}

// Validate validates this rectangle
func (m *Rectangle) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *Rectangle) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Rectangle) UnmarshalBinary(b []byte) error {
	var res Rectangle
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
