package file

import (
	"bitbucket.org/Axxonsoft/classifier/backend/model"
	"bitbucket.org/Axxonsoft/classifier/backend/models"
	"bitbucket.org/Axxonsoft/classifier/backend/store"
	"bitbucket.org/Axxonsoft/classifier/uploader/client"
	"bitbucket.org/Axxonsoft/classifier/uploader/client/file"
	"github.com/go-openapi/runtime"
	"io"
)

func NewService(store store.Store, uploader *client.Uploader) model.FileService {
	return &service{
		store:    store,
		uploader: uploader,
	}
}

type service struct {
	store    store.Store
	uploader *client.Uploader
}

func (s *service) UploadFile(name string, reader io.Reader) (*models.File, error) {
	params := file.NewFileUploadParams().
		WithName(name).
		WithFile(runtime.NamedReader("file", reader))

	created, err := s.uploader.File.FileUpload(params)
	return &models.File{
		ID:   created.Payload.ID,
		Name: created.Payload.Name,
		Size: created.Payload.Size,
	}, err
}
