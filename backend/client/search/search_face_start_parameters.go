// Code generated by go-swagger; DO NOT EDIT.

package search

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewSearchFaceStartParams creates a new SearchFaceStartParams object
// with the default values initialized.
func NewSearchFaceStartParams() *SearchFaceStartParams {
	var ()
	return &SearchFaceStartParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewSearchFaceStartParamsWithTimeout creates a new SearchFaceStartParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewSearchFaceStartParamsWithTimeout(timeout time.Duration) *SearchFaceStartParams {
	var ()
	return &SearchFaceStartParams{

		timeout: timeout,
	}
}

// NewSearchFaceStartParamsWithContext creates a new SearchFaceStartParams object
// with the default values initialized, and the ability to set a context for a request
func NewSearchFaceStartParamsWithContext(ctx context.Context) *SearchFaceStartParams {
	var ()
	return &SearchFaceStartParams{

		Context: ctx,
	}
}

// NewSearchFaceStartParamsWithHTTPClient creates a new SearchFaceStartParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewSearchFaceStartParamsWithHTTPClient(client *http.Client) *SearchFaceStartParams {
	var ()
	return &SearchFaceStartParams{
		HTTPClient: client,
	}
}

/*SearchFaceStartParams contains all the parameters to send to the API endpoint
for the search face start operation typically these are written to a http.Request
*/
type SearchFaceStartParams struct {

	/*Body*/
	Body SearchFaceStartBody

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the search face start params
func (o *SearchFaceStartParams) WithTimeout(timeout time.Duration) *SearchFaceStartParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the search face start params
func (o *SearchFaceStartParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the search face start params
func (o *SearchFaceStartParams) WithContext(ctx context.Context) *SearchFaceStartParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the search face start params
func (o *SearchFaceStartParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the search face start params
func (o *SearchFaceStartParams) WithHTTPClient(client *http.Client) *SearchFaceStartParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the search face start params
func (o *SearchFaceStartParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the search face start params
func (o *SearchFaceStartParams) WithBody(body SearchFaceStartBody) *SearchFaceStartParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the search face start params
func (o *SearchFaceStartParams) SetBody(body SearchFaceStartBody) {
	o.Body = body
}

// WriteToRequest writes these params to a swagger request
func (o *SearchFaceStartParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if err := r.SetBodyParam(o.Body); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
