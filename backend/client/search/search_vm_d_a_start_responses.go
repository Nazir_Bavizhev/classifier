// Code generated by go-swagger; DO NOT EDIT.

package search

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"strconv"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/Axxonsoft/classifier/backend/models"
)

// SearchVMDAStartReader is a Reader for the SearchVMDAStart structure.
type SearchVMDAStartReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *SearchVMDAStartReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewSearchVMDAStartCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewSearchVMDAStartCreated creates a SearchVMDAStartCreated with default headers values
func NewSearchVMDAStartCreated() *SearchVMDAStartCreated {
	return &SearchVMDAStartCreated{}
}

/*SearchVMDAStartCreated handles this case with default header values.

SearchVMDA vmda search
*/
type SearchVMDAStartCreated struct {
}

func (o *SearchVMDAStartCreated) Error() string {
	return fmt.Sprintf("[POST /search/vmda][%d] searchVmDAStartCreated ", 201)
}

func (o *SearchVMDAStartCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

/*SearchVMDAStartBody search VM d a start body
swagger:model SearchVMDAStartBody
*/
type SearchVMDAStartBody struct {

	// begin time
	// Format: date-time
	BeginTime strfmt.DateTime `json:"beginTime,omitempty"`

	// end time
	// Format: date-time
	EndTime strfmt.DateTime `json:"endTime,omitempty"`

	// Figures is an array of shapes (zones or line)
	Figures []*models.Figure `json:"figures"`

	// object properties
	ObjectProperties *models.ObjectProperties `json:"objectProperties,omitempty"`

	// QueryType can be zone, line or transition
	// Required: true
	QueryType *string `json:"queryType"`

	// video source Id
	// Required: true
	VideoSourceID *string `json:"videoSourceId"`
}

// Validate validates this search VM d a start body
func (o *SearchVMDAStartBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateBeginTime(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateEndTime(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateFigures(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateObjectProperties(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateQueryType(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateVideoSourceID(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *SearchVMDAStartBody) validateBeginTime(formats strfmt.Registry) error {

	if swag.IsZero(o.BeginTime) { // not required
		return nil
	}

	if err := validate.FormatOf("Body"+"."+"beginTime", "body", "date-time", o.BeginTime.String(), formats); err != nil {
		return err
	}

	return nil
}

func (o *SearchVMDAStartBody) validateEndTime(formats strfmt.Registry) error {

	if swag.IsZero(o.EndTime) { // not required
		return nil
	}

	if err := validate.FormatOf("Body"+"."+"endTime", "body", "date-time", o.EndTime.String(), formats); err != nil {
		return err
	}

	return nil
}

func (o *SearchVMDAStartBody) validateFigures(formats strfmt.Registry) error {

	if swag.IsZero(o.Figures) { // not required
		return nil
	}

	for i := 0; i < len(o.Figures); i++ {
		if swag.IsZero(o.Figures[i]) { // not required
			continue
		}

		if o.Figures[i] != nil {
			if err := o.Figures[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("Body" + "." + "figures" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (o *SearchVMDAStartBody) validateObjectProperties(formats strfmt.Registry) error {

	if swag.IsZero(o.ObjectProperties) { // not required
		return nil
	}

	if o.ObjectProperties != nil {
		if err := o.ObjectProperties.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("Body" + "." + "objectProperties")
			}
			return err
		}
	}

	return nil
}

func (o *SearchVMDAStartBody) validateQueryType(formats strfmt.Registry) error {

	if err := validate.Required("Body"+"."+"queryType", "body", o.QueryType); err != nil {
		return err
	}

	return nil
}

func (o *SearchVMDAStartBody) validateVideoSourceID(formats strfmt.Registry) error {

	if err := validate.Required("Body"+"."+"videoSourceId", "body", o.VideoSourceID); err != nil {
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (o *SearchVMDAStartBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *SearchVMDAStartBody) UnmarshalBinary(b []byte) error {
	var res SearchVMDAStartBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
