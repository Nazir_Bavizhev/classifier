// Code generated by go-swagger; DO NOT EDIT.

package user

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/Axxonsoft/classifier/backend/models"
)

// UserGetReader is a Reader for the UserGet structure.
type UserGetReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *UserGetReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewUserGetOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewUserGetUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewUserGetOK creates a UserGetOK with default headers values
func NewUserGetOK() *UserGetOK {
	return &UserGetOK{}
}

/*UserGetOK handles this case with default header values.

User response for user
*/
type UserGetOK struct {
	Payload *models.User
}

func (o *UserGetOK) Error() string {
	return fmt.Sprintf("[GET /users/{userId}][%d] userGetOK  %+v", 200, o.Payload)
}

func (o *UserGetOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.User)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewUserGetUnauthorized creates a UserGetUnauthorized with default headers values
func NewUserGetUnauthorized() *UserGetUnauthorized {
	return &UserGetUnauthorized{}
}

/*UserGetUnauthorized handles this case with default header values.

unauthorized
*/
type UserGetUnauthorized struct {
}

func (o *UserGetUnauthorized) Error() string {
	return fmt.Sprintf("[GET /users/{userId}][%d] userGetUnauthorized ", 401)
}

func (o *UserGetUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
