package store

import (
	"gopkg.in/stretchr/testify.v1/assert"
	"gopkg.in/stretchr/testify.v1/require"
	"testing"
)

func Test_Store(t *testing.T) {
	s := NewStore()
	defer s.Clean()
	s.SaveVideoSource(&VideoSource{ID: "hello", Name: "hi, there"})

	vs, err := s.GetVideoSourceByID("hello")
	require.NoError(t, err)
	assert.Equal(t, "hi, there", vs.Name)
	assert.Equal(t, "hello", vs.ID)

	vs, err = s.GetVideoSourceByID("hello2")
	require.NoError(t, err)
	assert.Nil(t, vs)
}
