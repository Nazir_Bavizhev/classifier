package store

import (
	"bitbucket.org/Axxonsoft/classifier/backend/models"
	"bitbucket.org/Axxonsoft/classifier/util"
	"github.com/asdine/storm"
	"github.com/asdine/storm/q"
	"github.com/coreos/bbolt"
	"os"
	"time"
)

type VideoSource struct {
	ID          string    `json:"id"`
	UserID      string    `json:"userId"`
	Name        string    `json:"name"`
	FileID      string    `json:"fileId"`
	HTTPLink    string    `json:"httpLink"`
	CreatedTime time.Time `json:"createdTime"`
}

type User struct {
	ID       string `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

// FileStore is an interface for db comands
type Store interface {
	SaveVideoSource(source *VideoSource) (*VideoSource, error)
	GetVideoSourceByID(id string) (*VideoSource, error)
	ListVideoSources(user *models.User, name *string, statuses []string, limit, offset int) (list []*VideoSource, total int64, err error)
	SaveUser(user *User) (*User, error)
	GetUserByEmail(email string) (*User, error)
	GetUserByID(id string) (*User, error)
	Clean()
}

// NewStore returns new store
func NewStore() Store {
	path := "./classifier.db"
	db, err := storm.Open(path, storm.BoltOptions(0777, &bolt.Options{Timeout: 1 * time.Second}))
	if err != nil {
		panic(err)
	}

	return &boltDBStore{db: db, path: path}
}

type boltDBStore struct {
	db   *storm.DB
	path string
}

func (s *boltDBStore) GetUserByID(id string) (user *User, err error) {
	user = &User{}

	if err = s.db.One("ID", id, user); err == storm.ErrNotFound {
		return nil, nil
	}
	return user, err
}

func (s *boltDBStore) GetUserByEmail(email string) (user *User, err error) {
	user = &User{}
	if err = s.db.One("Email", email, user); err == storm.ErrNotFound {
		return nil, nil
	}
	return user, err
}

func (s *boltDBStore) SaveUser(user *User) (*User, error) {
	return user, s.db.Save(user)
}

func (s *boltDBStore) GetVideoSourceByID(id string) (vs *VideoSource, err error) {
	vs = &VideoSource{}

	if err = s.db.One("ID", id, vs); err == storm.ErrNotFound {
		return nil, nil
	}
	return vs, err
}

func (s *boltDBStore) SaveVideoSource(source *VideoSource) (*VideoSource, error) {
	return source, s.db.Save(source)
}

func (s *boltDBStore) ListVideoSources(user *models.User, name *string, statuses []string, limit, offset int) (list []*VideoSource, total int64, err error) {
	matchers := []q.Matcher{q.Eq("UserID", user.ID)}
	if name != nil {
		matchers = append(matchers, q.NewFieldMatcher("Name", util.NewPrefixMatcher(*name)))
	}
	query := s.db.Select(matchers...)

	totalRecords, err := query.Count(&VideoSource{})
	if err != nil {
		return nil, 0, err
	}
	total = int64(totalRecords)

	list = make([]*VideoSource, 0)
	err = query.Limit(limit).Skip(offset).Find(&list)

	if err == storm.ErrNotFound {
		err = nil
	}

	return list, total, err
}

func (s *boltDBStore) Clean() {
	util.CheckErr(s.db.Close(), "s.db.Close()")
	util.CheckErr(os.Remove(s.path), "os.Remove(s.path)")
}
