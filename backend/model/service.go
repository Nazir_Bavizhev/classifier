package model

import (
	"bitbucket.org/Axxonsoft/classifier/backend/models"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations/user"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations/video_source"
	"bitbucket.org/Axxonsoft/classifier/backend/store"
	"io"
)

// VideoSourceService service interface
type VideoSourceService interface {
	CreateVideoSource(user *models.User, params *video_source.VideoSourceCreateParams) (*models.VideoSource, error)
	GetVideoSourceByID(id string) (*models.VideoSource, error)
	ListVideoSources(user *models.User, params *video_source.VideoSourceListParams) (*models.VideoSourceList, error)
}

type FileService interface {
	UploadFile(name string, reader io.Reader) (*models.File, error)
}

type UserService interface {
	CreateUser(params *user.UserCreateParams) (*models.User, error)
	LoginUser(params *user.UserLoginParams) (*models.AuthToken, error)
	GetToken(store.User) *models.AuthToken
	GetByTokenEnsure(*models.AuthToken) (*models.User, error)
	GetUser(params *user.UserGetParams) (*models.User, error)
}
