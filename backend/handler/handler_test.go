package handler

import (
	"bitbucket.org/Axxonsoft/classifier/backend/client/file"
	"bitbucket.org/Axxonsoft/classifier/backend/client/video_source"
	"bitbucket.org/Axxonsoft/classifier/backend/models"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi"
	"bitbucket.org/Axxonsoft/classifier/backend/store"
	file2 "bitbucket.org/Axxonsoft/classifier/uploader/client/file"
	testing2 "bitbucket.org/Axxonsoft/classifier/uploader/testing"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

var uploaderClient = testing2.Init()
var backendClient = Init(uploaderClient)

func TestVideoSource(t *testing.T) {
	user := createUser(t)

	uploadParams := file2.NewFileUploadParams().
		WithName("someName").
		WithFile(runtime.NamedReader("file", strings.NewReader("hello")))
	fileUploaded, err := uploaderClient.File.FileUpload(uploadParams)
	require.NoError(t, err)
	params := video_source.NewVideoSourceCreateParams().WithBody(&models.VideoSourceCreateParam{
		FileID:   fileUploaded.Payload.ID,
		Name:     "hi",
		HTTPLink: "123",
	})

	created, err := backendClient.VideoSource.VideoSourceCreate(params, newUserAuth(user))
	require.NoError(t, err)
	require.NotNil(t, created)
	assert.Equal(t, "hi", created.Payload.Name)
	assert.Equal(t, "123", created.Payload.HTTPLink)
	assert.Equal(t, fileUploaded.Payload.ID, created.Payload.FileID)
	assert.Equal(t, user.ID, created.Payload.UserID)

	list, err := backendClient.VideoSource.VideoSourceList(video_source.NewVideoSourceListParams(), newUserAuth(user))
	require.NoError(t, err)

	require.Len(t, list.Payload.Payload, 1)
	assert.Equal(t, int64(1), *list.Payload.Count)
	assert.Equal(t, int64(1), *list.Payload.Total)

	name := created.Payload.Name
	name = name[:len(name)-1]
	list, err = backendClient.VideoSource.VideoSourceList(video_source.NewVideoSourceListParams().WithName(&name), newUserAuth(user))
	require.NoError(t, err)

	require.Len(t, list.Payload.Payload, 1)
	assert.Equal(t, int64(1), *list.Payload.Count)
	assert.Equal(t, int64(1), *list.Payload.Total)

	name = "notExisted"
	list, err = backendClient.VideoSource.VideoSourceList(video_source.NewVideoSourceListParams().WithName(&name), newUserAuth(user))
	require.NoError(t, err)
	require.Empty(t, list.Payload.Payload)
}

func Test_VideoSource_ErrorIfFileNotExists(t *testing.T) {
	user := createUser(t)

	params := video_source.NewVideoSourceCreateParams().WithBody(&models.VideoSourceCreateParam{
		FileID:   "hello",
		Name:     "hi",
		HTTPLink: "123",
	})

	created, err := backendClient.VideoSource.VideoSourceCreate(params, newUserAuth(user))
	require.Error(t, err)
	require.IsType(t, err, video_source.NewVideoSourceCreateNotFound())
	require.Nil(t, created)
}

type userAuth struct {
	user store.User
}

func (a *userAuth) AuthenticateRequest(r runtime.ClientRequest, _ strfmt.Registry) error {
	r.SetHeaderParam("X-Auth-Token", restapi.UserService.GetToken(a.user).AuthToken)
	return nil
}

func TestUploader(t *testing.T) {
	user := createUser(t)

	upParams := file.NewFileUploadParams().WithName("newFile").WithFile(runtime.NamedReader("file", strings.NewReader("hithere!!!!!!")))
	created2, err := backendClient.File.FileUpload(upParams, newUserAuth(user))
	require.NoError(t, err)
	require.NotNil(t, created2)

	assert.Equal(t, "newFile", created2.Payload.Name)
	assert.Equal(t, int64(13), created2.Payload.Size)

	loaded, err := uploaderClient.File.FileGet(file2.NewFileGetParams().WithFileID(created2.Payload.ID))
	require.NoError(t, err)
	require.NotNil(t, loaded)

	assert.Equal(t, created2.Payload.Name, loaded.Payload.Name)
	assert.Equal(t, created2.Payload.Size, loaded.Payload.Size)
	assert.Equal(t, created2.Payload.ID, loaded.Payload.ID)
}

func Test_Uploader_ReturnsFileNotExists(t *testing.T) {
	loaded, err := uploaderClient.File.FileGet(file2.NewFileGetParams().WithFileID("someNotExistedID"))
	require.Error(t, err)
	require.Nil(t, loaded)

	assert.IsType(t, err, &file2.FileGetNotFound{})
}
