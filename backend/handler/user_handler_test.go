package handler

import (
	"bitbucket.org/Axxonsoft/classifier/backend/client/user"
	"bitbucket.org/Axxonsoft/classifier/backend/models"
	"bitbucket.org/Axxonsoft/classifier/backend/store"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestUserCreate(t *testing.T) {
	created := createUser(t)

	userGetParams := user.NewUserGetParams().WithUserID(created.ID)
	loaded, err := backendClient.User.UserGet(userGetParams, &userAuth{user: store.User{ID: created.ID}})
	require.NoError(t, err)
	require.NotNil(t, loaded)
	assert.Equal(t, created.ID, loaded.Payload.ID)

	userGetParams = user.NewUserGetParams().WithUserID("notExistedID")
	loaded, err = backendClient.User.UserGet(userGetParams, newUserAuth(created))
	require.IsType(t, err, user.NewUserGetUnauthorized())
	require.Nil(t, loaded)

	userGetParams = user.NewUserGetParams().WithUserID(created.ID)
	loaded, err = backendClient.User.UserGet(userGetParams, &userAuth{user: store.User{ID: "notExistedID"}})
	require.IsType(t, err, user.NewUserGetUnauthorized())
	require.Nil(t, loaded)

	userGetParams = user.NewUserGetParams().WithUserID(created.ID)
	loaded, err = backendClient.User.UserGet(userGetParams, &failAuth{})
	require.IsType(t, err, user.NewUserGetUnauthorized())
	require.Nil(t, loaded)
}

func newUserAuth(u *models.User) *userAuth {
	return &userAuth{user: store.User{ID: u.ID}}
}

func createUser(t *testing.T) *models.User {
	params := user.NewUserCreateParams().WithBody(&models.LoginParams{
		Email:    "some email",
		Password: "some password",
	})

	created, err := backendClient.User.UserCreate(params)
	require.NoError(t, err)
	require.NotNil(t, created)
	assert.Equal(t, "some email", created.Payload.Email)
	return created.Payload
}

type failAuth struct {
}

func (a *failAuth) AuthenticateRequest(r runtime.ClientRequest, _ strfmt.Registry) error {
	r.SetHeaderParam("X-Auth-Token", "invalid token")
	return nil
}
