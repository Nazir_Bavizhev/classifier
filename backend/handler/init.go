package handler

import (
	"bitbucket.org/Axxonsoft/classifier/backend/client"
	"bitbucket.org/Axxonsoft/classifier/backend/file"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations"
	"bitbucket.org/Axxonsoft/classifier/backend/store"
	"bitbucket.org/Axxonsoft/classifier/backend/user"
	"bitbucket.org/Axxonsoft/classifier/backend/videosource"
	client2 "bitbucket.org/Axxonsoft/classifier/uploader/client"
	doClient "bitbucket.org/Axxonsoft/classifier/util/client"
	"github.com/go-openapi/loads"
	"log"
)

func Init(uploader *client2.Uploader) *client.AxxonClassifier {
	backendSwagger, err := loads.Embedded(restapi.SwaggerJSON, restapi.FlatSwaggerJSON)
	if err != nil {
		log.Fatalln(err) // TODO
	}
	backendApi := operations.NewAxxonClassifierAPI(backendSwagger)

	backendServer := restapi.NewServer(backendApi)
	backendServer.ConfigureAPI()
	backend := backendServer.GetHandler()

	backendDoClient := doClient.New(client.DefaultHost, client.DefaultBasePath, client.DefaultSchemes)
	backendDoClient.WithDO(doClient.NewDO(backend).Do)

	backendClient := client.New(backendDoClient, nil)

	stor := store.NewStore()
	restapi.VideoSourceService = videosource.NewVideoSourceService(stor, uploader)
	restapi.UserService = user.NewService(stor)
	restapi.FileService = file.NewService(stor, uploader)

	return backendClient
}
