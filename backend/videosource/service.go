package videosource

import (
	"bitbucket.org/Axxonsoft/classifier/backend/model"
	"bitbucket.org/Axxonsoft/classifier/backend/models"
	"bitbucket.org/Axxonsoft/classifier/backend/restapi/operations/video_source"
	"bitbucket.org/Axxonsoft/classifier/backend/store"
	"bitbucket.org/Axxonsoft/classifier/uploader/client"
	"bitbucket.org/Axxonsoft/classifier/uploader/client/file"
	"bitbucket.org/Axxonsoft/classifier/util"
	"github.com/go-openapi/strfmt"
	"time"
)

// NewVideoSourceService returns new video source service
func NewVideoSourceService(store store.Store, uploader *client.Uploader) model.VideoSourceService {
	return &service{store: store, uploader: uploader}
}

type service struct {
	store    store.Store
	uploader *client.Uploader
}

func (s *service) GetVideoSourceByID(id string) (*models.VideoSource, error) {
	v, err := s.store.GetVideoSourceByID(id)
	if err != nil {
		return nil, err
	}
	return &models.VideoSource{
		ID:          v.ID,
		HTTPLink:    v.HTTPLink,
		FileID:      v.FileID,
		Name:        v.Name,
		CreatedTime: strfmt.DateTime(v.CreatedTime),
	}, nil
}

func (s *service) CreateVideoSource(user *models.User, params *video_source.VideoSourceCreateParams) (*models.VideoSource, error) {
	_, err := s.uploader.File.FileGet(file.NewFileGetParams().WithFileID(params.Body.FileID))
	if err != nil {
		return nil, err
	}

	v := &store.VideoSource{
		ID:          util.GetRandomID(),
		UserID:      user.ID,
		Name:        params.Body.Name,
		FileID:      params.Body.FileID,
		HTTPLink:    params.Body.HTTPLink,
		CreatedTime: time.Now(),
	}
	v, err = s.store.SaveVideoSource(v)
	if err != nil {
		return nil, err
	}

	return convertVideoSource(v), nil
}

func (s *service) ListVideoSources(user *models.User, params *video_source.VideoSourceListParams) (*models.VideoSourceList, error) {
	limit, offset := validateLimitOffset(params.Limit, params.Offset)
	list, total, err := s.store.ListVideoSources(user, params.Name, params.Status, limit, offset)
	if err != nil {
		return nil, err
	}
	count := int64(len(list))
	return &models.VideoSourceList{
		Total:   &total,
		Count:   &count,
		Payload: convertVideoSourceList(list),
	}, nil
}

func validateLimitOffset(Limit *int64, Offset *int64) (int, int) {
	limit := 1000
	offset := 0
	if Limit != nil && *Limit > 0 && *Limit < 1000 {
		limit = int(*Limit)
	}
	if Offset != nil && *Offset > 0 {
		offset = int(*Offset)
	}
	return limit, offset
}

func convertVideoSource(v *store.VideoSource) *models.VideoSource {
	return &models.VideoSource{
		ID:          v.ID,
		UserID:      v.UserID,
		HTTPLink:    v.HTTPLink,
		FileID:      v.FileID,
		Name:        v.Name,
		CreatedTime: strfmt.DateTime(v.CreatedTime),
	}
}

func convertVideoSourceList(vs []*store.VideoSource) []*models.VideoSource {
	res := make([]*models.VideoSource, len(vs))
	for i := 0; i < len(vs); i++ {
		res[i] = convertVideoSource(vs[i])
	}
	return res
}
