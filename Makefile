GO_IMAGE=golang:1.10.3
PROJECT_NAME=classifier
PROJECT_PACKAGE=bitbucket.org/Axxonsoft/$(PROJECT_NAME)
PROJECT_PATH=$$GOPATH/src/$(PROJECT_PACKAGE)

godep:
	go get -t github.com/tools/godep
	$(GOPATH)/bin/godep save ./...

godep-restore:
	go get github.com/tools/godep
	godep restore -v

check-gofmt:
	rm -r first.txt second.txt || true
	gofmt -d ./backend ./util ./uploader > first.txt
	touch second.txt
	diff -u first.txt second.txt
	rm -r first.txt second.txt || true 2>/dev/null

lint: check-gofmt
	go get github.com/alecthomas/gometalinter
	gometalinter --install
	gometalinter --concurrency=4 \
		--deadline 160s \
		--exclude=vendor \
		--dupl-threshold=60 \
		./...

	go get github.com/go-critic/go-critic/...
	gocritic check-project .

test:
	go test bitbucket.org/Axxonsoft/$(PROJECT_NAME)/...


GO_LINKER_FLAGS=-ldflags "-s \
	  -X $(PROJECT_PACKAGE)/config.PlanKey=$(bamboo_planKey) \
	  -X $(PROJECT_PACKAGE)/config.RepositoryBranch=$(bamboo_planRepository_branch) \
	  -X $(PROJECT_PACKAGE)/config.BuildNumber=$(bamboo_buildNumber) \
	  -X $(PROJECT_PACKAGE)/config.RepositoryRevision=$(bamboo_planRepository_revision)"

bin-buildd:
	docker run \
		-v $$GOPATH:/go \
		-e "bamboo_planKey=$(bamboo_planKey)" \
		-e "bamboo_planRepository_branch=$(bamboo_planRepository_branch)" \
		-e "bamboo_buildNumber=$(bamboo_buildNumber)" \
		-e "bamboo_planRepository_revision=$(bamboo_planRepository_revision)" \
		-w /go/src/$(PROJECT_PACKAGE) \
		-t $(GO_IMAGE) \
		make bin-build
	docker cp `docker ps -q -n=1`:/res $(PROJECT_PATH)
	echo $$GOPATH/bin

bin-build:
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build $(GO_LINKER_FLAGS) -a -installsuffix cgo -o /res/backend $(PROJECT_PACKAGE)/backend/cmd/axxon-classifier-server
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build $(GO_LINKER_FLAGS) -a -installsuffix cgo -o /res/uploader $(PROJECT_PACKAGE)/uploader/cmd/uploader-server


# ============== SWAGGER ===============

SWAGGER_IMAGE=quay.io/goswagger/swagger:0.16.0

swagger: swagger-backend swagger-uploader

swagger-backend: download-backend gen-backend-server gen-backend-client
swagger-uploader: download-uploader gen-uploader-client gen-uploader-server

gen-backend-server:
	docker run --rm -v $(GOPATH):/go/ -w /go/src/bitbucket.org/Axxonsoft/$(PROJECT_NAME) -t $(SWAGGER_IMAGE) \
		generate server \
		--target=backend \
		-f tools/backend.swagger.yml

gen-backend-client:
	docker run --rm -v $(GOPATH):/go/ -w /go/src/bitbucket.org/Axxonsoft/$(PROJECT_NAME) -t $(SWAGGER_IMAGE) \
		generate client \
		--existing-models=bitbucket.org/Axxonsoft/classifier/backend/models \
		--target=backend \
		-f tools/backend.swagger.yml


gen-uploader-server:
	docker run --rm -v $(GOPATH):/go/ -w /go/src/bitbucket.org/Axxonsoft/$(PROJECT_NAME) -t $(SWAGGER_IMAGE) \
		generate server \
		--target=uploader \
		-f tools/uploader.swagger.yml

gen-uploader-client:
	docker run --rm -v $(GOPATH):/go/ -w /go/src/bitbucket.org/Axxonsoft/$(PROJECT_NAME) -t $(SWAGGER_IMAGE) \
		generate client \
		--existing-models=bitbucket.org/Axxonsoft/classifier/uploader/models \
		--target=uploader \
		-f tools/uploader.swagger.yml

publish: swagger
	wget -O /dev/null \
		--header="Content-type: application/yaml" \
		--header="Authorization: eyJUb2tlblR5cGUiOiJBUEkiLCJzYWx0IjoiMDY2NjM4MTQtODExMS00YTZkLWJmODEtMmFmOGFjNmU5ZDBiIiwiYWxnIjoiSFM1MTIifQ.eyJqdGkiOiIuZ2l0aHViLjE0Mzk4NCIsImlhdCI6MTUzMzU3MjY5NX0.x0DsS4uPuzA1eYxYOSdlGOzLXfdGyIPNXrRm8KYG1Xe82h2J48y_0AE0v1tsUZiyvwU6cZS3BBikjkiYbtJdtw" \
		--post-file tools/backend.swagger.yml \
		https://api.swaggerhub.com/apis/itimofeev/classifier
	wget -O /dev/null \
		--header="Content-type: application/yaml" \
		--header="Authorization: eyJUb2tlblR5cGUiOiJBUEkiLCJzYWx0IjoiMDY2NjM4MTQtODExMS00YTZkLWJmODEtMmFmOGFjNmU5ZDBiIiwiYWxnIjoiSFM1MTIifQ.eyJqdGkiOiIuZ2l0aHViLjE0Mzk4NCIsImlhdCI6MTUzMzU3MjY5NX0.x0DsS4uPuzA1eYxYOSdlGOzLXfdGyIPNXrRm8KYG1Xe82h2J48y_0AE0v1tsUZiyvwU6cZS3BBikjkiYbtJdtw" \
		--post-file tools/uploader.swagger.yml \
		https://api.swaggerhub.com/apis/itimofeev/uploader
	echo 'https://app.swaggerhub.com/apis/itimofeev/uploader/1.0.0'

download-backend:
	wget -O tools/backend.swagger.yml  \
		--header="Authorization: eyJUb2tlblR5cGUiOiJBUEkiLCJzYWx0IjoiMDY2NjM4MTQtODExMS00YTZkLWJmODEtMmFmOGFjNmU5ZDBiIiwiYWxnIjoiSFM1MTIifQ.eyJqdGkiOiIuZ2l0aHViLjE0Mzk4NCIsImlhdCI6MTUzMzU3MjY5NX0.x0DsS4uPuzA1eYxYOSdlGOzLXfdGyIPNXrRm8KYG1Xe82h2J48y_0AE0v1tsUZiyvwU6cZS3BBikjkiYbtJdtw" \
		https://api.swaggerhub.com/apis/itimofeev/classifier/1.0.0/swagger.yaml

download-uploader:
	wget -O tools/uploader.swagger.yml  \
		--header="Authorization: eyJUb2tlblR5cGUiOiJBUEkiLCJzYWx0IjoiMDY2NjM4MTQtODExMS00YTZkLWJmODEtMmFmOGFjNmU5ZDBiIiwiYWxnIjoiSFM1MTIifQ.eyJqdGkiOiIuZ2l0aHViLjE0Mzk4NCIsImlhdCI6MTUzMzU3MjY5NX0.x0DsS4uPuzA1eYxYOSdlGOzLXfdGyIPNXrRm8KYG1Xe82h2J48y_0AE0v1tsUZiyvwU6cZS3BBikjkiYbtJdtw" \
		https://api.swaggerhub.com/apis/itimofeev/uploader/1.0.0/swagger.yaml

#	docker run -v /Users/ilyatimofee/prog/axxonsoft/src/bitbucket.org/Axxonsoft/$(PROJECT_NAME)/tools:/tools swaggerapi/swagger-codegen-cli generate -i /tools/backendswagger.json -l swagger-yaml -o /tools
# https://goswagger.io/
# https://github.com/go-swagger/go-swagger/blob/master/examples/task-tracker/swagger.yml
# https://app.swaggerhub.com/apis/itimofeev/uploader/1.0.0
# https://app.swaggerhub.com/apis/itimofeev/classifier/1.0.0
# ============== SWAGGER ===============

DOCKER_SWARM_MANAGER_IP=192.168.99.100


docker-rm:
	docker service rm $(shell docker service ls -q) || true

# --force-rm=true
docker-build:
	docker build  \
		-t classifier \
		-f $(PROJECT_PATH)/tools/backend.Dockerfile $$GOPATH
	docker build  \
		-t uploader \
		-f $(PROJECT_PATH)/tools/uploader.Dockerfile $$GOPATH

docker-run:
	docker stack deploy --compose-file tools/docker-stack.yml cl

rerun: docker-build docker-rm docker-run