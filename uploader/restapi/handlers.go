package restapi

import (
	"bitbucket.org/Axxonsoft/classifier/uploader/model"
	"bitbucket.org/Axxonsoft/classifier/uploader/models"
	"bitbucket.org/Axxonsoft/classifier/uploader/restapi/operations/file"
	"bitbucket.org/Axxonsoft/classifier/util"
	"github.com/go-openapi/runtime/middleware"
	"log"
	"net/http"
)

var FileService model.FileService
var FileFileUploadHandler = file.FileUploadHandlerFunc(func(params file.FileUploadParams) middleware.Responder {
	info, err := FileService.SaveFile(params.Name, params.File)
	if err != nil {
		log.Fatal(err) //TODO
	}
	return file.NewFileUploadCreated().WithPayload(&models.File{
		ID:   info.ID,
		Size: info.Size,
		Name: info.OriginalName,
	})
})

var FileFileGetHandler = file.FileGetHandlerFunc(func(params file.FileGetParams) middleware.Responder {
	info, err := FileService.GetFileInfo(params.FileID)
	if err != nil {
		return convertError(err)
	}
	return file.NewFileGetOK().WithPayload(&models.File{
		ID:   info.ID,
		Size: info.Size,
		Name: info.OriginalName,
	})
})

func convertError(err error) middleware.Responder {
	if he, ok := err.(*util.HTTPError); ok {
		switch he.Code {
		case http.StatusNotFound:
			return file.NewFileGetNotFound()
		}
	}
	return middleware.NotImplemented("hello")
}
