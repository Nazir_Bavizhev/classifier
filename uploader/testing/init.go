package testing

import (
	"bitbucket.org/Axxonsoft/classifier/uploader/bolt"
	client2 "bitbucket.org/Axxonsoft/classifier/uploader/client"
	file2 "bitbucket.org/Axxonsoft/classifier/uploader/file"
	"bitbucket.org/Axxonsoft/classifier/uploader/local"
	"bitbucket.org/Axxonsoft/classifier/uploader/restapi"
	"bitbucket.org/Axxonsoft/classifier/uploader/restapi/operations"
	"bitbucket.org/Axxonsoft/classifier/util/client"
	"github.com/go-openapi/loads"
	"log"
)

func Init() *client2.Uploader {
	swaggerSpec, err := loads.Embedded(restapi.SwaggerJSON, restapi.FlatSwaggerJSON)
	if err != nil {
		log.Fatalln(err)
	}
	api := operations.NewUploaderAPI(swaggerSpec)
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	handler := server.GetHandler()

	c := client.New(client2.DefaultHost, client2.DefaultBasePath, client2.DefaultSchemes)
	do := client.NewDO(handler)
	c.WithDO(do.Do)

	store := local.NewStore("/Users/ilyatimofee/prog/go/classifier/src/bitbucket.org/Axxonsoft/classifier/tools/temp")
	db := bolt.NewStore()
	restapi.FileService = file2.NewService(store, db)

	return client2.New(c, nil)
}
