package testing

import (
	"bitbucket.org/Axxonsoft/classifier/uploader/client/file"
	"github.com/go-openapi/runtime"
	"gopkg.in/stretchr/testify.v1/assert"
	"gopkg.in/stretchr/testify.v1/require"
	"strings"
	"testing"
)

func TestClient(t *testing.T) {
	upClient := Init()

	p := file.NewFileUploadParams().WithName("someName").WithFile(runtime.NamedReader("hi", strings.NewReader("hellothere")))
	r, err := upClient.File.FileUpload(p)
	require.NoError(t, err)

	require.NotNil(t, r.Payload)
	assert.Equal(t, "someName", r.Payload.Name)
	assert.Equal(t, int64(10), r.Payload.Size)
}
