package main

import (
	"bitbucket.org/Axxonsoft/classifier/uploader/bolt"
	"bitbucket.org/Axxonsoft/classifier/uploader/file"
	"bitbucket.org/Axxonsoft/classifier/uploader/local"
	"bitbucket.org/Axxonsoft/classifier/uploader/restapi"
	"bitbucket.org/Axxonsoft/classifier/util"
	"github.com/kelseyhightower/envconfig"
)

type config struct {
	LocalStorePath string `envconfig:"local_store_path" required:"true"`
}

func init() {
	var config config
	err := envconfig.Process("uploader", &config)
	util.CheckErr(err, "envconfig")

	store := local.NewStore(config.LocalStorePath)
	db := bolt.NewStore()
	restapi.FileService = file.NewService(store, db)
}
