// Package api uploader
//
// Axxon Uploader API
//
//     Schemes: http
//     Host: localhost
//     BasePath: /uploader/api/v1
//     Version: 1.0.0
//     Contact: Ilya Timofeev<ilya.timofeev@ru.axxonsoft.com> http://axxonsoft.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta
package model

import (
	"io"
	"time"
)

type FileStore interface {
	WriteFile(key string, reader io.Reader) (int64, error)
	GetFile(key string) (io.Reader, error)
}

type FileService interface {
	SaveFile(name string, reader io.Reader) (*FileInfo, error)
	GetFile(id string) (io.Reader, error)
	GetFileInfo(id string) (*FileInfo, error)
}

type DBStore interface {
	SaveFileInfo(info *FileInfo) (*FileInfo, error)
	GetFileInfoEnsure(id string) (*FileInfo, error)
	Clear()
}

type FileInfo struct {
	ID           string
	CreateTime   time.Time
	Size         int64
	OriginalName string
}
