package bolt

import (
	"bitbucket.org/Axxonsoft/classifier/uploader/model"
	"bitbucket.org/Axxonsoft/classifier/util"
	"fmt"
	"github.com/asdine/storm"
	"github.com/coreos/bbolt"
	"os"
	"time"
)

// NewStore returns new store
func NewStore() model.DBStore {
	path := "./uploader.db"
	db, err := storm.Open(path, storm.BoltOptions(0777, &bolt.Options{Timeout: 1 * time.Second}))
	if err != nil {
		panic(err)
	}

	return &store{db: db, path: path}
}

type store struct {
	db   *storm.DB
	path string
}

func (s *store) SaveFileInfo(info *model.FileInfo) (*model.FileInfo, error) {
	return info, s.db.Save(info)
}

func (s *store) GetFileInfoEnsure(id string) (*model.FileInfo, error) {
	info := &model.FileInfo{}
	err := s.db.One("ID", id, info)
	if err == storm.ErrNotFound {
		return nil, util.NewNotFoundError(fmt.Sprintf("File not found by id %s", id))
	}
	return info, nil
}

func (s *store) Clear() {
	util.CheckErr(s.db.Close(), "s.db.Close()")
	util.CheckErr(os.Remove(s.path), "os.Remove(s.path)")
}
