package file

import (
	"bitbucket.org/Axxonsoft/classifier/uploader/model"
	"bitbucket.org/Axxonsoft/classifier/util"
	"io"
	"time"
)

func NewService(store model.FileStore, db model.DBStore) model.FileService {
	return &service{
		store: store,
		db:    db,
	}
}

type service struct {
	store model.FileStore
	db    model.DBStore
}

func (s *service) SaveFile(name string, reader io.Reader) (info *model.FileInfo, err error) {
	info = &model.FileInfo{
		ID:           util.GetRandomID(),
		CreateTime:   time.Now(),
		OriginalName: name,
	}

	info.Size, err = s.store.WriteFile(info.ID, reader)
	if err != nil {
		return nil, err
	}

	return s.db.SaveFileInfo(info)
}

func (s *service) GetFile(id string) (io.Reader, error) {
	info, err := s.db.GetFileInfoEnsure(id)
	if err != nil {
		return nil, err
	}
	return s.store.GetFile(info.ID)
}

func (s *service) GetFileInfo(id string) (*model.FileInfo, error) {
	return s.db.GetFileInfoEnsure(id)
}
