package file

import (
	"bitbucket.org/Axxonsoft/classifier/uploader/bolt"
	"bitbucket.org/Axxonsoft/classifier/uploader/local"
	"gopkg.in/stretchr/testify.v1/assert"
	"gopkg.in/stretchr/testify.v1/require"
	"io/ioutil"
	"strings"
	"testing"
)

func TestSaveAndGetFile(t *testing.T) {
	db := bolt.NewStore()
	defer db.Clear()
	store := local.NewStore("/Users/ilyatimofee/prog/go/classifier/src/bitbucket.org/Axxonsoft/classifier/tools/temp")
	s := NewService(store, db)

	info, err := s.SaveFile("someName", strings.NewReader("hello"))
	require.NoError(t, err)
	assert.Equal(t, "someName", info.OriginalName)

	r, err := s.GetFile(info.ID)
	require.NoError(t, err)

	data, err := ioutil.ReadAll(r)
	require.NoError(t, err)

	assert.Equal(t, "hello", string(data))
	assert.Equal(t, int64(5), info.Size)
}
