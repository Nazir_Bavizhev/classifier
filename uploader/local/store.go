package local

import (
	"bitbucket.org/Axxonsoft/classifier/uploader/model"
	"bufio"
	"fmt"
	"io"
	"os"
)

func NewStore(path string) model.FileStore {
	return &store{
		path: path,
	}
}

type store struct {
	path string
}

func (s *store) WriteFile(key string, reader io.Reader) (int64, error) {
	f, err := os.Create(fmt.Sprintf("%s/%s", s.path, key))
	if err != nil {
		return 0, err
	}
	defer f.Close()

	return io.Copy(f, reader)
}

func (s *store) GetFile(key string) (io.Reader, error) {
	f, err := os.Open(fmt.Sprintf("%s/%s", s.path, key))
	if err != nil {
		return nil, err
	}
	//TODO f.close

	return bufio.NewReader(f), nil
}
