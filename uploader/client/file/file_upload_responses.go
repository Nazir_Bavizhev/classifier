// Code generated by go-swagger; DO NOT EDIT.

package file

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/Axxonsoft/classifier/uploader/models"
)

// FileUploadReader is a Reader for the FileUpload structure.
type FileUploadReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *FileUploadReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewFileUploadCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewFileUploadCreated creates a FileUploadCreated with default headers values
func NewFileUploadCreated() *FileUploadCreated {
	return &FileUploadCreated{}
}

/*FileUploadCreated handles this case with default header values.

FileResponse description of uploaded file
*/
type FileUploadCreated struct {
	Payload *models.File
}

func (o *FileUploadCreated) Error() string {
	return fmt.Sprintf("[POST /file][%d] fileUploadCreated  %+v", 201, o.Payload)
}

func (o *FileUploadCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.File)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
