package util

import (
	"fmt"
	"github.com/go-errors/errors"
	"github.com/go-openapi/runtime"
	"net/http"
	"runtime/debug"
)

// HTTPError represents an error that occurred while handling a request.
type HTTPError struct {
	Code        int    `json:"-"`
	ErrorKey    string `json:"error"`
	Description string `json:"description"`
	Inner       error  `json:"inner"`
	Stacktrace  string `json:"stacktrace"`
}

// Error makes it compatible with `error` interface.
func (he *HTTPError) Error() string {
	return fmt.Sprintf("code=%d, key=%v", he.Code, he.ErrorKey)
}

// NewHTTPError creates a new HTTPError instance.
func NewHTTPError(code int, key, description string, inner ...error) *HTTPError {
	he := &HTTPError{
		Code:        code,
		ErrorKey:    key,
		Description: description,
	}
	if len(inner) > 0 {
		he.Inner = inner[0]
		he.Description = he.Description + ": " + he.Inner.Error()
		if withTrace, ok := inner[0].(*errors.Error); ok {
			he.Stacktrace = withTrace.ErrorStack()
		}
	}
	if len(he.Stacktrace) == 0 {
		he.Stacktrace = string(debug.Stack())
	}

	return he
}

func (he *HTTPError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {
	if he.Code > 0 {
		rw.WriteHeader(he.Code)
	} else {
		rw.WriteHeader(http.StatusInternalServerError)
	}
	if err := producer.Produce(rw, he); err != nil {
		panic(err)
	}
}

func NewNotFoundError(description string) *HTTPError {
	return NewHTTPError(http.StatusNotFound, "err.not.found", description)
}

func NewUnauthorized(description string) *HTTPError {
	return NewHTTPError(http.StatusUnauthorized, "err.unauthorized", description)
}
