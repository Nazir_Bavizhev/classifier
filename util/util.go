package util

import (
	"github.com/rs/xid"
	"log"
)

// GetRandomID returns globally unique ID
func GetRandomID() string {
	return xid.New().String() // TODO replace with https://github.com/lithammer/shortuuid
}

// CheckErr check error is nil and if not panic with message
func CheckErr(err error, msg ...string) {
	if err != nil {
		log.Panicln(msg, err)
	}
}
