package util

import (
	"errors"
	"github.com/asdine/storm/q"
	"strings"
)

func NewPrefixMatcher(prefix string) q.FieldMatcher {
	return &prefixMatcher{
		prefix: prefix,
	}
}

type prefixMatcher struct {
	prefix string
}

func (m *prefixMatcher) MatchField(v interface{}) (bool, error) {
	strDB, ok := v.(string)
	if !ok {
		return false, errors.New("unexpected value type")
	}
	return strings.HasPrefix(strDB, m.prefix), nil
}
